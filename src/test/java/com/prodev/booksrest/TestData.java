package com.prodev.booksrest;

import com.prodev.booksrest.domain.Book;
import com.prodev.booksrest.domain.BookEntity;

public class TestData {
    private TestData(){
    }

    public static Book testBook() {
        return Book.builder()
                .isbn("02345678")
                .author("Virginia Woolf")
                .title("The Waves")
                .build();
    }

    public static BookEntity testBookEntity() {
        return BookEntity.builder()
                .isbn("02345678")
                .author("Virginia Woolf")
                .title("The Waves")
                .build();
    }
}
